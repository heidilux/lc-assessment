<?php

namespace App\Http\Controllers;

use DB;
use App\Sale;
use App\Weather;
use Carbon\Carbon;
use App\Http\Requests;

class SalesController extends Controller
{

    /**
     * API key for weatherunderground
     * @var string
     */
    private $apiKey;

    /**
     * Base URI for weatherunderground API
     * @var string
     */
    private $baseUri;

    /**
     * Instantiate the controller and set some properties
     */
    public function __construct()
    {
        // must be logged in to access anything
        $this->middleware('auth');

        $this->apiKey = getenv('API_KEY');
        $this->baseUri = 'http://api.wunderground.com/';
    }


    /**
     * Prepare some data and display the Sales page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSales()
    {
        // Grab various info to populate the Sales by Type table
        $typeSales = $this->getSalesTypeData();

        // load 'resources/views/sales.blade.php
        return view('sales', compact('typeSales'));
    }

    /**
     * Prepare some data and display the Raw Sales page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRawData()
    {
        // SELECT * FROM sales e.g.;
        $sales = Sale::all();

        // load 'resources/views/raw-sales.blade.php
        return view('raw-sales', compact('sales'));
    }

    /**
     * Used this to populate DB with weather data, 10 at a time
     */
    public function seed()
    {
        $dates = [
            20150830,
            20150831
        ];

        foreach ($dates as $date)
        {
            $this->getWeatherData($date);
        }
    }

    /**
     * Fetch weather data from API and persist to DB
     *
     * @param $date
     * @return static
     */
    private function getWeatherData($date)
    {
        $data = $this->fetchFromApi($date, 'OH', 'Columbus');

        // Grab specific data from the returned json object
        $info = $data->{'history'}->{'dailysummary'};
        $avgTemp = $info[0]->{'meantempi'};
        $minTemp = $info[0]->{'mintempi'};
        $maxTemp = $info[0]->{'maxtempi'};
        $thunder = $info[0]->{'thunder'};
        $precipAmt = $info[0]->{'precipi'};
        $precip = $info[0]->{'rain'};
        $date = $info[0]->{'date'}->{'year'} .
            $info[0]->{'date'}->{'mon'} .
            $info[0]->{'date'}->{'mday'};

        // Place data in array with keys to match table columns
        $weatherData = [
            'avg_temp'      => $avgTemp,
            'min_temp'      => $minTemp,
            'max_temp'      => $maxTemp,
            'thunder'       => $thunder,
            'precip'        => $precip,
            'precip_amt'    => $precipAmt,
            'date'          => $date
        ];

        // Create new row in DB
        $entry = Weather::create($weatherData);

        return $entry;
    }

    /**
     * Make the API call and return json-ified data
     *
     * @param $date
     * @param $state
     * @param $city
     * @return json
     */
    private function fetchFromApi($date, $state, $city)
    {
        $res = file_get_contents(
            $this->baseUri . '/api/' .
            $this->apiKey . '/history_' .
            $date . '/q/' .
            $state . '/' .
            $city . '/.json');
        $parsed = json_decode($res);
        return $parsed;
    }

    /**
     * Fetch Sales data from DB, run a couple SUMs
     * Add some weather info to each member of the array
     *
     * @return array
     */
    private function getSalesTypeData()
    {
        $typeSales = DB::select(
            "SELECT sale_date AS 'sale_date',
            SUM(IF(sales_types_id = 1, amount, 0)) AS 'liquor',
            SUM(IF(sales_types_id = 2, amount, 0)) AS 'beer',
            SUM(IF(sales_types_id = 3, amount, 0)) AS 'wine',
            SUM(IF(sales_types_id = 4, amount, 0)) AS 'food',
            SUM(IF(sales_types_id = 5, amount, 0)) AS 'beverage',
            SUM(IF(sales_types_id = 6, amount, 0)) AS 'giftCard',
            SUM(IF(sales_types_id = 7, amount, 0)) AS 'retail',
            SUM(amount) AS Total
            FROM sales
            GROUP BY sale_date"
        );

        // Add additional data to the array for use in the view
        foreach ($typeSales as $typeSale) {

            // Grab the weather on the date of the sale
            $weather = Weather::where('date', '=', $typeSale->sale_date)->get();

            // Grab first (although only) result from the collection
            $weather = $weather->first();

            // Create Carbon instance from date field. Have to do this because I used
            // a raw DB query above instead of an eloquent query (which would return Carbon automagically)
            $date = Carbon::createFromFormat('Y-m-d', $typeSale->sale_date);

            // Add a 'weekend' flag so I can add a weekend class to table rows
            $typeSale->isWeekend = ($date->isWeekend()) ? 1 : 0;

            // Format the date for easier reading in table
            $typeSale->sale_date = $date->format('m-d-Y, l');

            // Include the weather observations
            $typeSale->avg_temp = $weather->avg_temp;
            $typeSale->precip = $weather->precip;
            $typeSale->thunder = $weather->thunder;
        }

        return $typeSales;
    }
}
