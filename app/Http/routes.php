<?php

// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes... commented out after setting up users
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');

// Pages
Route::get('sales', 'SalesController@showSales');
Route::get('raw-sales-data', 'SalesController@showRawData');


////////////////////////////////////////
//  Temporary routes used to seed DB  //
////////////////////////////////////////

//Route::get('seedTypes', function()
//{
//    $types = [
//        ['name' => 'Liquor'],
//        ['name' => 'Beer'],
//        ['name' => 'Wine'],
//        ['name' => 'Food'],
//        ['name' => 'Beverage'],
//        ['name' => 'Gift Card'],
//        ['name' => 'Retail']
//        ];
//
//    foreach ($types as $type)
//    {
//        App\SalesType::create($type);
//    }
//});

//Route::get('seedTenWeather', 'DashboardController@seed');