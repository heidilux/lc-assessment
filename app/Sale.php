<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    /**
     * Which fields are automagically converted to Carbon instances
     *
     * @var array
     */
    protected $dates = ['sale_date'];

    /**
     * A given sale has a type (e.g. Beer, Wine, etc)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\SalesType', 'sales_types_id');
    }

    /**
     * A given sale has a relationship in the weather table joined by the dates
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function weather()
    {
        return $this->belongsTo('App\Weather', 'sale_date', 'date');
    }

}
