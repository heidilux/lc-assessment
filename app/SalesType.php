<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesType extends Model
{

    /**
     * DB columns mass-assignable
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A given sale type can have many sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sale()
    {
        return $this->hasMany('App\Sales');
    }
}
