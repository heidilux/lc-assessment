<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    /**
     * The name of the table doesn't follow convention
     * (which would've been 'weathers') so it's specified explicitly
     *
     * @var string
     */
    protected $table = 'weather_data';

    /**
     * Mass assignable fields
     *
     * @var array
     */
    protected $fillable =[
        'avg_temp',
        'min_temp',
        'max_temp',
        'thunder',
        'precip',
        'precip_amt',
        'date'
    ];

    /**
     * A given weather entry can have many sales associated
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany('App\Sales', 'date', 'date');
    }
}
