@extends('layouts.master')

@section('content')

    <div class="row">
        <h1 class="text-center">Login</h1>

        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}

            <div class="auth-box col-md-4 col-md-offset-4">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default">Login</button>
                </div>
            </div>
        </form>
    </div>

    @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

@stop