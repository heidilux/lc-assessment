@extends('layouts.master')

@section('content')

    <div class="row">
        <h1 class="text-center">Register</h1>

        <form method="POST" action="/auth/register">
            {!! csrf_field() !!}

            <div class="auth-box col-md-4 col-md-offset-4">

                <div class="form-group">
                    <label for="email">Name:</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Confirm:</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default">Register</button>
                </div>
            </div>
        </form>
    </div>


    @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    @endif

@stop