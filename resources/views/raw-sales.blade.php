@extends('layouts.master')

@section('content')

    <h1>Raw Sales Data</h1>

    <table class="table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Sale Type</th>
            <th>Amount</th>
            <th>Temperature</th>
            <th>Rainy?</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($sales as $sale)
            <tr class="{{ ($sale->sale_date->day % 2) ? 'even-day' : 'odd-day' }}">
                <td>{{ $sale->sale_date->toFormattedDateString() }}</td>
                <td>{{ $sale->type->name }}</td>
                <td>{{ $sale->amount }}</td>
                <td>{{ $sale->weather->avg_temp }}</td>
                <td>{{ ($sale->weather->precip) ? 'Yes' : 'No' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop