@extends('layouts.master')

@section('content')

    <h1>Sales by Type</h1>

    <hr>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">Current Sales Data</div>
                <div class="panel-body">
                    There does appear to be some correlation between the weather and the
                    total sales for any given day. Most of the largest sales days occur
                    on or immediately after days with inclement weather. There are however,
                    a number of large sales days that do not fit into this pattern.
                    <br><br>
                    Knowing that inclement weather is imminent might inform a manager as to the
                    stock to have on hand, or perhaps the number of staff to have available.
                    The weather forecast could inform short-term specials or ad campaigns.
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">Additional information</div>
                <div class="panel-body">
                    There are number of large sales days that do not fit with the weather patterns.
                    Without information as to the rest of the calendar events, it is not possible to
                    correlate those days with a pattern.
                    <br><br>
                    It would be useful to have other information in order to help predict sales.
                    Perhaps there were sporting events, advertisement campaigns, community events,
                    weddings, etc.
                    <br>
                    <br>
                </div>
            </div>

        </div>
    </div>

    <hr>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Date</th>
            <th>Weather</th>
            <th>Liquor</th>
            <th>Beer</th>
            <th>Wine</th>
            <th>Food</th>
            <th>Beverage</th>
            <th>Gift Card</th>
            <th>Retail</th>
            <th>Total</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($typeSales as $typeSale)
            <tr class="{{ ($typeSale->isWeekend) ? 'weekend' : '' }}">
                <td>{{ $typeSale->sale_date }}</td>
                <td>{{ $typeSale->avg_temp }}&#176
                    {!! ($typeSale->precip) ? (($typeSale->thunder) ?
                    '<i class="wi wi-thunderstorm"><i/>' :
                        '<i class="wi wi-showers"><i/>') : '' !!}
                </td>
                <td>${{ number_format($typeSale->liquor, 2) }}</td>
                <td>${{ number_format($typeSale->beer, 2) }}</td>
                <td>${{ number_format($typeSale->wine, 2) }}</td>
                <td>${{ number_format($typeSale->food, 2) }}</td>
                <td>${{ number_format($typeSale->beverage, 2) }}</td>
                <td>${{ number_format($typeSale->giftCard, 2) }}</td>
                <td>${{ number_format($typeSale->retail, 2) }}</td>
                <td>${{ number_format($typeSale->Total, 2) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop